import path from "path";
import fs from "fs";

export const OrmAutoImport = () => {
  const modelsDir = path.join(__dirname, '/src/app/entities'); // Подставьте правильный путь
  const modelFiles = fs.readdirSync(modelsDir);

  const models: string[] = [];

  modelFiles.forEach((file) => {
    if (file.endsWith('.ts')) {
      models.push(file.replace('.ts', ''))
    }
  });

  let modelsStr = '';
  for(let i = 0; i < models.length; i++) {
    modelsStr += "'" + models[i] + "', "
  }

  let imports = "";
  models.forEach(async (model) => {
    imports += `import ${model} from "@/app/entities/${model}";\n`
  })

  imports += `\nexport const models = [${models.map((model) => model).join(', ')}];\n`

  const mainFilePath = path.join(__dirname, '/src/entities.ts')
  fs.writeFile(mainFilePath, imports, 'utf8', (err) => {
    if (err) {
      console.error('Error writing main.ts:', err);
      return;
    }

    console.log('Imports added to main.ts successfully.');
  });
}
