export default interface ApiMethodsInterface {
  get<T, P>(url: string, config?: P): Promise<T>;
  post<T, B, P>(url: string, data?: B, config?: P): Promise<T>;
  put<T, B, P>(url: string, data?: B, config?: P): Promise<T>;
  patch<T, B, P>(url: string, data?: B, config?: P): Promise<T>;
  delete<T, P>(url: string, config?: P): Promise<T>;
}
