import BadRequestErrorDTO from "@/app/dto/response/BadRequestErrorDTO"
import { Axios, AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import ApiMethodsInterface from "@/app/api/repositories/ApiMethodsInterface";
import ApiRepositoryInterface from "@/app/api/repositories/ApiRepositoryInterface";
import ErrorDTO from "@/app/dto/response/ErrorDTO";
import UnauthorizedErrorDTO from "@/app/dto/response/UnauthorizedErrorDTO";

export default abstract class ApiRepository implements ApiRepositoryInterface, ApiMethodsInterface {
  protected httpProvider: AxiosInstance

  public constructor() {
    this.httpProvider = this.getHttpProvider({
      baseURL: import.meta.env.VITE_APP_API_URL
    })
  }

  getHttpProvider(options: AxiosRequestConfig): object {
    return new Axios(options)
  }

  delete<T, P>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    return this.request('delete', url, config)
  }

  get<T, P>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    return this.request('get', url, config)
  }

  patch<T, B, P>(url: string, data?: B, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    return this.request('patch', url, data, config)
  }

  post<T, B, P>(url: string, data?: B, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    return this.request('post', url, data, {
      ...config, headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    })
  }

  put<T, B, P>(url: string, data?: B, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    return this.request('put', url, data, config)
  }

  private async request(type: string, url: string, data?: B, config?: AxiosRequestConfig): Promise<AxiosResponse | ErrorDTO> {
    const resultConfig = {
      method: type,
      url,
      ...config
    }
    if (data) resultConfig.data = data

    const response =  await this.httpProvider.request(resultConfig)

    const responseData = JSON.parse(response.data)

    switch (response.status) {
      case 401:
        return Promise.reject(new UnauthorizedErrorDTO(responseData.code, responseData.message))
      case 400:
        return Promise.reject(new BadRequestErrorDTO(responseData.code, responseData.message, responseData.violations))
      default:
        return Promise.resolve(responseData)
    }
  }
}
