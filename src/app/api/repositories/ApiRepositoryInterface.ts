import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default interface ApiRepositoryInterface {

  /**
   * Gets http provider for current class context
   */
  getHttpProvider(options: any): object;

  request(type: string, ...args: any): Promise<any | ErrorDTO>;
}
