import ApiRepository from "@/app/api/repositories/ApiRepository";
import AuthenticateInterfaceDTO from "@/app/dto/auth/AuthenticateInterfaceDTO"
import RegistrationInterfaceDTO from "@/app/dto/auth/RegistrationInterfaceDTO"
import TokenResponseDTO from "@/app/dto/response/TokenResponseDTO";
import ErrorDTO from "@/app/dto/response/ErrorDTO";
import AuthRepositoryInterface from "@/app/api/repositories/auth/AuthRepositoryInterface";

class AuthRepository extends ApiRepository implements AuthRepositoryInterface {
  async authenticate(userData: AuthenticateInterfaceDTO): Promise<TokenResponseDTO | ErrorDTO> {
      try {
          const result = await this.post('/auth', JSON.stringify(userData))
          return Promise.resolve(new TokenResponseDTO(result.token, result.refresh_token))
      } catch (e: ErrorDTO) {
          return Promise.reject(e)
      }
  }

  async registration(userData: RegistrationInterfaceDTO) {
    try {
      const result = await this.post('/api/users', JSON.stringify(userData))
      return Promise.resolve(result)
    } catch (error: ErrorDTO) {
      return Promise.reject(error)
    }
  }
}

export default new AuthRepository()
