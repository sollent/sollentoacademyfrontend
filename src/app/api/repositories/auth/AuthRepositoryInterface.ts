import AuthenticateInterfaceDTO from "@/app/dto/auth/AuthenticateInterfaceDTO"
import RegistrationInterfaceDTO from "@/app/dto/auth/RegistrationInterfaceDTO"
import ErrorDTO from "@/app/dto/response/ErrorDTO";

interface AuthRepositoryInterface  {
  authenticate(userData: AuthenticateInterfaceDTO): Promise<any | ErrorDTO>;

  registration(userData: RegistrationInterfaceDTO): Promise<any | ErrorDTO>;
}

export default AuthRepositoryInterface;
