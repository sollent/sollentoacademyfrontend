import ChatRepositoryInterface from "@/app/api/repositories/chat/ChatRepositoryInterface";
import ApiRepository from "@/app/api/repositories/ApiRepository";
import ErrorDTO from "@/app/dto/response/ErrorDTO";
import ChatDTO from "@/app/dto/chat/ChatDTO";
import UserDTO from "@/app/dto/UserDTO";
import ChatMessageDTO from "@/app/dto/chat/chat-message/ChatMessageDTO";
import ChatTextMessageDTO from "@/app/dto/chat/chat-message/ChatTextMessageDTO";

export default new class ChatRepository extends ApiRepository implements ChatRepositoryInterface {
    async findChats(): Promise<ChatDTO[] | ErrorDTO> {
        try {
            const result = await this.get('/api/chats');
            return Promise.resolve(result.items.map(item => {
                return new ChatDTO(
                    item.id,
                    this.createUserDTOFromResponse(item.creator),
                    this.createUserDTOFromResponse(item.invitedUser)
                )
            }))
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }

    async findChat(chatId: string): Promise<ChatDTO, ErrorDTO> {
        try {
            const result = await this.get(`/api/chats/${chatId}`);
            return Promise.resolve(new ChatDTO(
                item.id,
                this.createUserDTOFromResponse(result.creator),
                this.createUserDTOFromResponse(result.invitedUser),
                this.createChatMessagesDTOsFromResponse(result.messages)
            ))
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }

    private createUserDTOFromResponse(responseItem: any): UserDTO {
        return new UserDTO(responseItem.id, responseItem.email);
    }

    private createChatMessagesDTOsFromResponse(responseItems: any): ChatMessageDTO[] {
        // only for text messages! should be extended in the future
        return responseItems.map(item => {
            return new ChatTextMessageDTO(
                item.id,
                this.createUserDTOFromResponse(item.receiver),
                this.createUserDTOFromResponse(item.sender),
                item.text
            )
        })
    }
}
