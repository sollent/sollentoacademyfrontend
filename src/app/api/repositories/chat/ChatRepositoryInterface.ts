import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default interface ChatRepositoryInterface {
    findChats(): Promise<any | ErrorDTO>
    findChat(chatId: string): Promise<any, ErrorDTO>;
}
