import ApiRepository from "@/app/api/repositories/ApiRepository";
import UserRepositoryInterface from "@/app/api/repositories/user/UserRepositoryInterface";
import UserDTO from "@/app/dto/UserDTO";
import ErrorDTO from "@/app/dto/response/ErrorDTO";
import ChatDTO from "@/app/dto/chat/ChatDTO";

export default new class UserRepository extends ApiRepository implements UserRepositoryInterface {
    async findUsers(): Promise<UserDTO[] | ErrorDTO> {
        try {
            const result = await this.get('/api/users');
            return Promise.resolve(result.items.map(item =>  new UserDTO(
              item.id, item.email, true
            )))
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }
}
