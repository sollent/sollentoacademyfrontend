import UserDTO from "@/app/dto/UserDTO";
import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default interface UserRepositoryInterface {
    findUsers(): Promise<UserDTO[] | ErrorDTO>;
}
