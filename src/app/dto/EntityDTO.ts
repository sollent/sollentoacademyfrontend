import {id} from "vuetify/locale";

export default abstract class EntityDTO {
    private id: string;

    constructor(id: string) {
        this.id = id;
    }

    getId(): string {
        return this.id;
    }
}
