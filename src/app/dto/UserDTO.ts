import {id} from "vuetify/locale";
import EntityDTO from "@/app/dto/EntityDTO";

export default class UserDTO extends EntityDTO {
    private email: string;
    private online: boolean;

    constructor(
      id: string,
      email: string,
      online: boolean
    ) {
        super(id);

        this.id = id;
        this.email = email;
        this.online = online;
    }

    getEmail(): string {
        return this.email;
    }

    isOnline(): boolean {
        return this.online;
    }
}
