export default interface AuthenticateInterfaceDTO {
  email: string;
  password: string;
}
