export default interface RegistrationInterfaceDTO {
  email: string;
  plainPassword: string;
  userType: string;
}
