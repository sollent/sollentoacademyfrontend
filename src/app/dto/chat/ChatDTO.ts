import UserDTO from "@/app/dto/UserDTO";
import EntityDTO from "@/app/dto/EntityDTO";
import ChatMessageDTO from "@/app/dto/chat/chat-message/ChatMessageDTO";
import {id} from "vuetify/locale";

export default class ChatDTO extends EntityDTO {
    private creator: UserDTO;
    private invitedUser: UserDTO;
    private messages: ChatMessageDTO[];

    constructor(id: string, creator: UserDTO, invitedUser: UserDTO, messages: ChatMessageDTO[] = []) {
        super(id);
        this.creator = creator;
        this.invitedUser = invitedUser;
        this.messages = messages;
    }

    getCreator(): UserDTO {
        return this.creator;
    }

    getInvitedUser(): UserDTO {
        return this.invitedUser;
    }

    getMessages(): ChatMessageDTO[] {
        return this.messages;
    }
}
