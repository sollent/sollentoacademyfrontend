import EntityDTO from "@/app/dto/EntityDTO";
import UserDTO from "@/app/dto/UserDTO";

export default abstract class ChatMessageDTO extends EntityDTO {
    private receiver: UserDTO;
    private sender: UserDTO;

    /**
     * Iri
     */
    private chat: string;


    constructor(id: string, receiver: UserDTO, sender: UserDTO, chat: string) {
        super(id);

        this.receiver = receiver;
        this.sender = sender;
        this.chat = chat;
    }

    getReceiver(): UserDTO {
        return this.receiver;
    }

    getSender(): UserDTO {
        return this.sender;
    }
}
