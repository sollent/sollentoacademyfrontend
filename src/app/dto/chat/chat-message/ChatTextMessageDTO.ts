import ChatMessageDTO from "@/app/dto/chat/chat-message/ChatMessageDTO";
import UserDTO from "@/app/dto/UserDTO";

export default class ChatTextMessageDTO extends ChatMessageDTO {
    private text: string;

    constructor(id: string, receiver: UserDTO, sender: UserDTO, chat: string, text: string) {
        super(id, receiver, sender, chat);

        this.text = text;
    }

    getText(): string {
        return this.text;
    }
}
