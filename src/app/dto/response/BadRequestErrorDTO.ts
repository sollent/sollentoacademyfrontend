import ErrorDTO from "@/app/dto/response/ErrorDTO";
import {Record} from "@vuex-orm/core";

export default class BadRequestErrorDTO extends ErrorDTO {
    private violations: Record<string, []>

    constructor(code: Number, message: string, violations: Record<string, []>) {
        super(code, message);
        this.violations = violations;
    }

    getViolations(): Record<string, []> {
        return this.violations
    }
}
