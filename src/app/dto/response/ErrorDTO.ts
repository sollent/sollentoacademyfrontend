export default abstract class ErrorDTO {
    private code: number;
    private message: string;


    constructor(code: Number, message: string) {
        this.code = code;
        this.message = message;
    }

    getCode(): number {
        return this.code
    }

    getMessage(): string {
        return this.message
    }
}
