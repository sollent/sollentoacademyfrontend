export default class TokenResponseDTO {
    private token: string;
    private refreshToken: string;

    constructor(token: string, refreshToken: string) {
        this.token = token;
        this.refreshToken = refreshToken;
    }

    getToken(): string {
        return this.token
    }

    getRefreshToken(): string {
        return this.refreshToken
    }
}
