import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default class UnauthorizedErrorDTO extends ErrorDTO {
}
