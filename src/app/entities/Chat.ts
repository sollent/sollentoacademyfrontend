import { Model } from '@vuex-orm/core'
import User from "@/app/entities/User";

export default class Chat extends Model {
    static entity = 'chats'

    static fields () {
        return {
            id: this.attr(null),
            creator_id: this.attr(null),
            creator: this.belongsTo(User, 'creator_id'),
            invitedUser_id: this.attr(null),
            invitedUser: this.belongsTo(User, 'invitedUser_id'),
            messages: this.attr(''),
        }
    }
}
