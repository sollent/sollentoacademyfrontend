import { Model } from '@vuex-orm/core'
import User from "@/app/entities/User";

export default abstract class ChatMessage extends Model {
    static entity = 'chat_messages'

    static fields () {
        return {
            id: this.attr(null),
            receiver: this.belongsTo(User, 'id'),
            sender: this.belongsTo(User, 'id'),
        }
    }
}
