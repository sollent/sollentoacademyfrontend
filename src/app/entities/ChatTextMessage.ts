import { Model } from '@vuex-orm/core'
import User from "@/app/entities/User";

export default class ChatTextMessage extends Model {
    static entity = 'chatTextMessages'

    static fields () {
        return {
            id: this.attr(null),
            receiver: this.belongsTo(User, 'id'),
            sender: this.belongsTo(User, 'id'),
            text: this.attr(''),
        }
    }
}
