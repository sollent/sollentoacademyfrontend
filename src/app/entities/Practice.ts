import { Model } from '@vuex-orm/core'

export default class Practice extends Model {
    static entity = 'practices'

    static fields () {
        return {
            id: this.attr(null),
            name: this.attr(''),
        }
    }
}
