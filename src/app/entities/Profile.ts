import { Model } from '@vuex-orm/core'

export default class Profile extends Model {
  static entity = 'profiles'

  static fields() {
    return {
      id: this.attr(''),
      profile_id: this.attr(null),
      firstName: this.attr(''),
      lastName: this.attr(''),
      avatar: this.attr('')
    }
  }
}
