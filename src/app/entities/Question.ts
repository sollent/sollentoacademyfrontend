import { Model } from '@vuex-orm/core'

export default class Question extends Model {
    static entity = 'questions'

    static fields () {
        return {
            id: this.attr(null),
            name: this.attr(''),
        }
    }
}
