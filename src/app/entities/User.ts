import { Model } from '@vuex-orm/core'
import Profile from "@/app/entities/Profile"

export default class User extends Model {
  static entity = 'users'

  static fields () {
    return {
      id: this.attr(''),
      email: this.attr(''),
      online: this.attr(false),
      profile: this.hasMany(Profile, 'profile_id')
    }
  }
}
