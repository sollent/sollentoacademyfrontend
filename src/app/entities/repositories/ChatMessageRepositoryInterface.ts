import ChatMessage from "@/app/entities/ChatMessage";
import ChatMessageDTO from "@/app/dto/chat/chat-message/ChatMessageDTO";

export default interface ChatMessageRepositoryInterface {
    findBy(ids: string[]): ChatMessage[];
    saveSeveral(chatMessageDTOs: ChatMessageDTO[]): ChatMessage[];
    findMessagesOrCreateByDTOs(chatMessagesDTOs: ChatMessageDTO[]): ChatMessage[];
}
