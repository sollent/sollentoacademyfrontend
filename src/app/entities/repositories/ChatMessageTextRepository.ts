import ChatMessageRepositoryInterface from "@/app/entities/repositories/ChatMessageRepositoryInterface";
import ChatMessageDTO from "@/app/dto/chat/chat-message/ChatMessageDTO";
import UserRepositoryInterface from "@/app/entities/repositories/UserRepositoryInterface";
import UserRepository from "@/app/entities/repositories/UserRepository";
import UserServiceInterface from "@/app/services/user/UserServiceInterface";
import UserService from "@/app/services/user/UserService";
import ChatTextMessage from "@/app/entities/ChatTextMessage";
import ChatTextMessageDTO from "@/app/dto/chat/chat-message/ChatTextMessageDTO";

export default new class ChatMessageTextRepository implements ChatMessageRepositoryInterface {

    private userRepository: UserRepositoryInterface;
    private userService: UserServiceInterface

    constructor() {
        this.userRepository = UserRepository
        this.userService = UserService
    }

    findBy(ids: string[]): ChatTextMessage[] {
        return ChatTextMessage.query().where('id', ids).select()
    }

    async saveSeveral(chatMessageDTOs: ChatMessageDTO[]): ChatTextMessage[] {
        return await ChatTextMessage.insert({
            data: chatMessageDTOs.map((chatMessageDTO: ChatMessageDTO) => {
                return {
                    id: chatMessageDTO.getId(),
                    receiver: this.userRepository.findUserOrCreateByDTO(chatMessageDTO.getReceiver()),
                    sender: this.userRepository.findUserOrCreateByDTO(chatMessageDTO.getSender())
                }
            })
        })
    }

    findMessagesOrCreateByDTOs(chatMessagesDTOs: ChatTextMessageDTO[]): ChatTextMessage[] {
        const messages = ChatTextMessage.findIn(chatMessagesDTOs.map(dto => dto.getId()))

        if (messages.length > 0) return messages;

        return this.saveSeveral(chatMessagesDTOs);
    }
}
