import ChatRepositoryInterface from "@/app/entities/repositories/ChatRepositoryInterface";
import ChatDTO from "@/app/dto/chat/ChatDTO";
import Chat from "@/app/entities/Chat";
import UserRepositoryInterface from "@/app/entities/repositories/UserRepositoryInterface";
import UserRepository from "@/app/entities/repositories/UserRepository";
import UserServiceInterface from "@/app/services/user/UserServiceInterface";
import UserService from "@/app/services/user/UserService";
import ChatMessageRepositoryInterface from "@/app/entities/repositories/ChatMessageRepositoryInterface";
import ChatMessageTextRepository from "@/app/entities/repositories/ChatMessageTextRepository";

export default new class ChatRepository implements ChatRepositoryInterface {
    private userRepository: UserRepositoryInterface
    private userService: UserServiceInterface
    private chatMessageRepository: ChatMessageRepositoryInterface;

    constructor() {
        this.userRepository = UserRepository
        this.userService = UserService
        this.chatMessageRepository = ChatMessageTextRepository;
    }

    async save(chatDTO: ChatDTO): Chat {
        const chats = await Chat.insert({
            data: {
                id: chatDTO.getId(),
                creator: this.userRepository.findUserOrCreateByDTO(chatDTO.getCreator()),
                invitedUser: this.userRepository.findUserOrCreateByDTO(chatDTO.getInvitedUser()),
                messages: this.chatMessageRepository.findMessagesOrCreateByDTOs(chatDTO.getMessages()),
            }
        })

        return chats[0];
    }

    async saveSeveral(chatDTOs: ChatDTO[]): Chat[] {
        await Chat.insert({
            data: chatDTOs.map((chatDTO: ChatDTO) => {
                return {
                    id: chatDTO.getId(),
                    creator: this.userRepository.findUserOrCreateByDTO(chatDTO.getCreator()),
                    invitedUser: this.userRepository.findUserOrCreateByDTO(chatDTO.getInvitedUser()),
                    messages: this.chatMessageRepository.findMessagesOrCreateByDTOs(chatDTO.getMessages())
                }
            })
        })

        return Chat.query().with('creator').with('invitedUser').all();
    }

    findAll(): Chat[] {
        return Chat.all();
    }
}
