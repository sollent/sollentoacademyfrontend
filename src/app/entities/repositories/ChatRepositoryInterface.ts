import ChatDTO from "@/app/dto/chat/ChatDTO";
import Chat from "@/app/entities/Chat";

export default interface ChatRepositoryInterface {
    save(chatDTO: ChatDTO): Chat;
    saveSeveral(chatDTOs: ChatDTO[]): Chat[];
    findAll(): Chat[];
}
