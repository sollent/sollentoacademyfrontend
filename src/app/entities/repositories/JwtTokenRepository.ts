import TokenRepositoryInterface from "@/app/entities/repositories/TokenRepositoryInterface";
import JwtTokenService from "@/app/services/JwtTokenService";
import TokenResponseDTO from "@/app/dto/response/TokenResponseDTO";

export default new class JwtTokenRepository implements TokenRepositoryInterface {
    private jwtTokenService: JwtTokenService;

    constructor() {
        this.jwtTokenService = JwtTokenService;
    }

    async saveToken(tokenData: TokenResponseDTO): Promise<void> {
        this.jwtTokenService.saveToken(tokenData);
    }
}
