import UserRepositoryInterface from "@/app/entities/repositories/UserRepositoryInterface";
import UserDTO from "@/app/dto/UserDTO";
import User from "@/app/entities/User";
import {id} from "vuetify/locale";

export default new class UserRepository implements UserRepositoryInterface {

    async save(userDTO: UserDTO): User {
       const users = await User.insert({
            data: {
                id: userDTO.getId(),
                email: userDTO.getEmail(),
                online: userDTO.isOnline()
            }
        })

        return users[0]
    }


    async saveSeveral(userDTOs: UserDTO[]): User[] {
       await User.insert({
            data: userDTOs.map((userDTO: UserDTO) => {
                return {
                    id: userDTO.getId(),
                    email: userDTO.getEmail(),
                    online: userDTO.isOnline()
                }
            })
        })

        return User.all()
    }

    find(id: string): User | null {
        return User.find(id)
    }

    findAll(): User[] {
        return User.all();
    }

    findUserOrCreateByDTO(userDTO: UserDTO): User {
        return this.find(userDTO.getId()) ?? this.save(userDTO)
    }
}
