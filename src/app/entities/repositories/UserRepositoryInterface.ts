import UserDTO from "@/app/dto/UserDTO";
import User from "@/app/entities/User";

export default interface UserRepositoryInterface {
    save(userDTO: UserDTO): User;
    saveSeveral(userDTOs: UserDTO[]): User[];
    find(id: string): User | null;
    findAll(): User[];
    findUserOrCreateByDTO(userDTO: UserDTO): User;
}
