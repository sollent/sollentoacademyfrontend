import TokenResponseDTO from "@/app/dto/response/TokenResponseDTO"
import StorageServiceInterface from "@/app/services/StorageServiceInterface"
import TokenServiceInterface from "@/app/services/TokenServiceInterface"

import LocalStorageService from "@/app/services/LocalStorageService"

enum TOKENS {
  ACCESS = 'token',
  REFRESH = 'refreshToken'
}

export default new class JwtTokenService implements TokenServiceInterface{
    private localStorageService: StorageServiceInterface;

    constructor() {
        this.localStorageService = LocalStorageService;
    }

    public saveToken(token: TokenResponseDTO): void {
        this.localStorageService.update(TOKENS.ACCESS, token.getToken());
        this.localStorageService.update(TOKENS.REFRESH, token.getRefreshToken());
    }

    public isTokenExists(): boolean {
      return !!this.localStorageService.get(TOKENS.ACCESS) && !!this.localStorageService.get(TOKENS.REFRESH)
    }
}
