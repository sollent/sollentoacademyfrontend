import StorageServiceInterface from "@/app/services/StorageServiceInterface";

export default new class LocalStorageService implements StorageServiceInterface {
    public save(key: string, value: string): void {
        if (localStorage.getItem(key) === null) {
            localStorage.setItem(key, value)
        }
    }
    public update(key: string, value: string): void {
        localStorage.setItem(key, value)
    }

    public get(key: string): string | null {
      return localStorage.getItem(key)
    }
}
