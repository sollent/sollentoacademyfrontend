export default interface StorageServiceInterface {
    save(key: string, value: any): void;
    update(key: string, value: any): void;
    get(key: string): string | null;
}
