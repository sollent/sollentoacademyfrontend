import TokenResponseDTO from "@/app/dto/response/TokenResponseDTO"

export default interface TokenServiceInterface {
  saveToken(token: TokenResponseDTO): void;
  isTokenExists(): boolean;
}
