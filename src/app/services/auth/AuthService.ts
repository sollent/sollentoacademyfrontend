import AuthenticateInterfaceDTO from "@/app/dto/auth/AuthenticateInterfaceDTO"
import RegistrationInterfaceDTO from "@/app/dto/auth/RegistrationInterfaceDTO"
import TokenResponseDTO from "@/app/dto/response/TokenResponseDTO"
import ErrorDTO from "@/app/dto/response/ErrorDTO"

import AuthRepository from "@/app/api/repositories/auth/AuthRepository"
import TokenRepositoryInterface from "@/app/entities/repositories/TokenRepositoryInterface"
import JwtTokenRepository from "@/app/entities/repositories/JwtTokenRepository"

import AuthServiceInterface from "@/app/services/auth/AuthServiceInterface"
import JwtTokenService from "@/app/services/JwtTokenService"
import TokenServiceInterface from "@/app/services/TokenServiceInterface"
import AuthRepositoryInterface from "@/app/api/repositories/auth/AuthRepositoryInterface";

export default new class AuthService implements AuthServiceInterface {
  private tokenService: TokenServiceInterface;
  private tokenRepository: TokenRepositoryInterface;
  private authRepository: AuthRepositoryInterface;

  constructor() {
    this.tokenService = JwtTokenService;
    this.tokenRepository = JwtTokenRepository;
    this.authRepository = AuthRepository;
  }

  async auth(userData: AuthenticateInterfaceDTO): Promise<boolean | ErrorDTO> {
    try {
      const tokenResponseDTO: TokenResponseDTO | ErrorDTO = await this.authRepository.authenticate(userData)
      this.tokenRepository.saveToken(tokenResponseDTO);
      return Promise.resolve(true)
    } catch (error: ErrorDTO) {
      // do something
      return Promise.reject(error)
    }
  }

  async registration(userData: RegistrationInterfaceDTO): Promise<boolean | ErrorDTO> {
    try {
      const result = await this.authRepository.registration(userData)
      return Promise.resolve(true)
    } catch (error: ErrorDTO) {
      // do something
      return Promise.reject(error)
    }
  }

  public isAuthenticated(): boolean {
    const isTokenExist = this.tokenService.isTokenExists()

    // Add API check

    return isTokenExist;

  }
}
