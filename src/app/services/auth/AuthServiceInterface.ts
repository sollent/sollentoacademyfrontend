export default interface AuthServiceInterface {
    auth<T>(data: any): Promise<T>;
    registration<T>(data: any): Promise<T>;
    isAuthenticated(): boolean;
}
