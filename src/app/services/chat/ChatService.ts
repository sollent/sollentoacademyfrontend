import ChatServiceInterface from "@/app/services/chat/ChatServiceInterface";
import Chat from "@/app/entities/Chat";
import ChatApiRepositoryInterface from "@/app/api/repositories/chat/ChatRepositoryInterface";
import ChatApiRepository from "@/app/api/repositories/chat/ChatRepository";
import ChatRepositoryInterface from "@/app/entities/repositories/ChatRepositoryInterface";
import ChatRepository from "@/app/entities/repositories/ChatRepository";
import ChatDTO from "@/app/dto/chat/ChatDTO";
import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default new class ChatService implements ChatServiceInterface {
    private chatApiRepository: ChatApiRepositoryInterface
    private chatEntityRepository: ChatRepositoryInterface

    constructor() {
        this.chatApiRepository = ChatApiRepository
        this.chatEntityRepository = ChatRepository
    }

    async getMyChats(): Promise<Chat[] | ErrorDTO> {
        try {
            const chatDTOs: ChatDTO[] = await this.chatApiRepository.findChats();
            return Promise.resolve(this.chatEntityRepository.saveSeveral(chatDTOs));
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }

    async getChat(id: string): Promise<Chat | ErrorDTO> {
        try {
            const chatDTO: ChatDTO = await this.chatApiRepository.findChat(id);
            return Promise.resolve(this.chatEntityRepository.save(chatDTO));
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }
}
