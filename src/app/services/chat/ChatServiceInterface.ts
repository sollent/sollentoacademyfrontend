import Chat from "@/app/entities/Chat";

export default interface ChatServiceInterface {
    getMyChats(): Promise<Chat[]>;
}
