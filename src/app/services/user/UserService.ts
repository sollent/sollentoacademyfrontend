import UserServiceInterface from "@/app/services/user/UserServiceInterface";
import UserDTO from "@/app/dto/UserDTO";
import User from "@/app/entities/User";
import UserApiRepositoryInterface from "@/app/api/repositories/user/UserRepositoryInterface";
import UserApiRepository from "@/app/api/repositories/user/UserRepository";
import UserRepositoryInterface from "@/app/entities/repositories/UserRepositoryInterface";
import UserRepository from "@/app/entities/repositories/UserRepository";
import ErrorDTO from "@/app/dto/response/ErrorDTO";

export default new class UserService implements UserServiceInterface {

    private userApiRepository: UserApiRepositoryInterface;
    private userEntityRepository: UserRepositoryInterface

    constructor() {
        this.userApiRepository = UserApiRepository
        this.userEntityRepository = UserRepository
    }

    async getUsers(): Promise<User[]> {
        try {
            const userDTOs: UserDTO[] = await this.userApiRepository.findUsers();
            return Promise.resolve( this.userEntityRepository.saveSeveral(userDTOs));
        } catch (e: ErrorDTO) {
            return Promise.reject(e)
        }
    }
}
