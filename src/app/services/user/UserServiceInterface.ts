import UserDTO from "@/app/dto/UserDTO";
import User from "@/app/entities/User";

export default interface UserServiceInterface {
    getUsers(): Promise<any>;
}
