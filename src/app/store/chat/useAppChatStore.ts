import ErrorDTO from "@/app/dto/response/ErrorDTO"
import User from "@/app/entities/User"
import UserService from "@/app/services/user/UserService"

interface AppChatStateInterface {
  users: User[]
}

export const useAppChatStore = defineStore('appchat', {
  state: (): AppChatStateInterface => ({
    users: [],
  }),
  actions: {
    saveUsersState(users: User[]) {
      this.users = users
      console.log(this.users)
    },

    fetchUsers() {
      UserService
        .getUsers()
        .then((users: User[]) => this.saveUsersState(users))
        .catch((error: ErrorDTO) => console.error(error))
    }
  }
})
