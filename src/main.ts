import '@/@fake-db/db'
import ability from '@/plugins/casl/ability'
import i18n from '@/plugins/i18n'
import layoutsPlugin from '@/plugins/layouts'
import vuetify from '@/plugins/vuetify'
import { loadFonts } from '@/plugins/webfontloader'
import router from '@/router'
import { abilitiesPlugin } from '@casl/vue'
import '@core/scss/template/index.scss'
import '@styles/styles.scss'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from '@/App.vue'
import { models } from "@/entities"
import Pusher from "pusher-js";
import {ORMDatabase} from "vuex-orm-decorators";

const pusher = new Pusher('14fdf368cecb352c274b', { cluster: 'eu' })
pusher.subscribe('chat')
pusher.bind('NEW_MESSAGE', data => {
  console.log(data)
})

loadFonts()

// Create vue app
const app = createApp(App)

// Create a new instance of Database.
const database = new VuexORM.Database()
models.forEach((model) => database.register(model))

// Create Vuex Store and register database through Vuex ORM.
const store = new Vuex.Store({ plugins: [VuexORM.install(database)] })

app
  .use(store)
  .use(createPinia())
  .use(vuetify)

  .use(router)
  .use(layoutsPlugin)
  .use(i18n)
  .use(abilitiesPlugin, ability, { useGlobalProperties: true })

  .mount('#app')
